/**
 * A class to hold hourly employees.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class Hourly extends Employee {
    private int hoursWorked;

    public Hourly(String inputName, String inputAddress, String inputPhone, String inputSocial, double inputPayRate) {
        super(inputName, inputAddress, inputPhone, inputSocial, inputPayRate);

        hoursWorked = 0;
    }

    public void addHours(int inputHours) {
        hoursWorked += inputHours;
    }

    public double pay() {
        double payment = payRate * hoursWorked;

        hoursWorked = 0;

        return payment;
    }

    public String toString() {
        String result = "";

        result += super.toString();

        result += "Current hours: " + hoursWorked + "\n";

        return result;
    }
}