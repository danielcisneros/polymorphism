/**
 * A class to hold executive information
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class Executive extends Employee {
    private double bonus;

    public Executive (String inputName, String inputAddress, String inputPhone, String inputSocial, double inputPayRate){
        super(inputName, inputAddress, inputPhone, inputSocial, inputPayRate);

        bonus = 0.0;
    }

    public void awardBonus(double myBonus){
        bonus = myBonus;
    }

    public double pay(){
        double payment = super.pay() + bonus;

        bonus = 0.0;

        return payment;
    }
 }