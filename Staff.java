/**
 * A class to call all the polymorphic stuff.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class Staff {
    private StaffMember[] staffList;

    public Staff() {
        staffList = new StaffMember[6];

        staffList[0] = new Executive("Rick", "123 Main st.", "555-1234", "111-1111", 6074);

        staffList[1] = new Employee("Morty", "123 Main st.", "555-1234", "222-2222", 20);

        staffList[2] = new Employee("Summer", "123 Main st.", "555-1234", "333-3333", 50);

        staffList[3] = new Hourly("Pickle Rick", "123 Pickle Jar", "999-4569", "444-4444", 2000000);

        staffList[4] = new Volunteer("Phineas", "Tri State Area", "456-1234)");

        staffList[5] = new Volunteer("Furb", "Tri State Area", "123-7894");

        ((Executive) staffList[0]).awardBonus(600.00);

        ((Hourly) staffList[3]).addHours(29);
    }

    public void payday () {
        double amount;

        for (int count = 0; count < staffList.length; count++){

            System.out.println(staffList[count]);

            amount = staffList[count].pay(); //polymorphic

            if (amount == 0.0)
                System.out.println("Thanks");
            else
                System.out.println("Paid: " + amount);
                
            System.out.println("-----------------------------------------------");

        }
    }

}