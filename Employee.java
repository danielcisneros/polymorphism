/**
 * A class to hold employees
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class Employee extends StaffMember {
    protected String socialSecNum;
    protected double payRate;

    public Employee(String inputName, String inputAddress, String inputPhone, String inputSocNum, double inputPayRate) {
        super(inputName, inputAddress, inputPhone);

        socialSecNum = inputSocNum;
        payRate = inputPayRate;
    }

    public String toString() {
        String result = "";

        result += super.toString();

        result += "Social Security Num: " + socialSecNum + "\n";

        return result;
    }

    public double pay() {
        return payRate;
    }

}