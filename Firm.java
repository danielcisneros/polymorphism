/**
 * 
 * A firm class to drive the entire program.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class Firm {
     public static void main(String[] args) {
         
     Staff personal = new Staff();

     personal.payday();
         
     }
 }