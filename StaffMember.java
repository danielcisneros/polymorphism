/**
 * A generic class to hold staff members.
 * 
 * @author Daniel Cisneros II
 * @version 1.0
 * 
 */

public abstract class StaffMember {
    protected String name;
    protected String address;
    protected String phone;

    public StaffMember(String inputName, String inputAddress, String inputPhone) {
        name = inputName;
        address = inputAddress;
        phone = inputPhone;
    }

    public String toString() {
        String result = "";

        result += "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone + "\n";

        return result;
    }

    public abstract double pay();
}