/**
 * A class to hold volunteers
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class Volunteer extends StaffMember{

    public Volunteer(String inputName, String inputAddress, String inputPhone){
        super(inputName, inputAddress, inputPhone);
    }

    public double pay(){
        return 0.0;
    }
 }